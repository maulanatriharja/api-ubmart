<?php
header( 'Content-Type: application/json' );
require_once '../setting/connection.php';

$query = 'SELECT t1.*,t2.nama_daerah FROM desa t1 LEFT JOIN daerah t2 ON t1.id_daerah=t2.id';

$result = mysqli_query( $conn, $query ) or die( 'Select Query Failed.' );

$i = 0;

while( $rows = mysqli_fetch_array( $result ) ) {
    $array_data[$i]['id'] = $rows['id'];
    $array_data[$i]['nama_desa'] = $rows['nama_desa'];

    $array_data[$i]['daerah']['id_daerah'] = $rows['id_daerah'];
    $array_data[$i]['daerah']['nama_daerah'] = $rows['nama_daerah'];

    $array_data[$i]['created_at'] = $rows['created_at'];
    $array_data[$i]['updated_at'] = $rows['updated_at'];
    
    $i ++;
}

echo json_encode( $array_data );

?>
