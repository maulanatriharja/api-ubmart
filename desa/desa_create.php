<?php
header( 'Content-Type: application/json' );
require_once '../setting/connection.php' ;

$id_daerah = addslashes( htmlentities( $_POST['id_daerah'] ) );
$nama_desa = addslashes( htmlentities( $_POST['nama_desa'] ) );

$query = "INSERT INTO desa SET id_daerah='$id_daerah', nama_desa='$nama_desa'";

$result = mysqli_query( $conn, $query );

$response = array();

if ( $result ) {
    echo json_encode( array( 'message' => 'Sukses menambah data baru.', 'status' => true ) );
} else {
    echo json_encode( array( 'message' => 'Gagal menambah data baru.', 'status' => false ) );
}

?>
