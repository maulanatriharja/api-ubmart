<?php
header( 'Content-Type: application/json' );
require_once '../setting/connection.php' ;

$nama_daerah = addslashes( htmlentities( $_POST['nama_daerah'] ) );

$query = "INSERT INTO daerah SET nama_daerah='$nama_daerah'";

$result = mysqli_query( $conn, $query );

$response = array();

if ( $result ) {
    echo json_encode( array( 'message' => 'Sukses menambah data baru.', 'status' => true ) );
} else {
    echo json_encode( array( 'message' => 'Gagal menambah data baru.', 'status' => false ) );
}

?>
