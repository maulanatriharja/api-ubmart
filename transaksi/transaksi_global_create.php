<?php
header( 'Content-Type: application/json' );
require_once '../setting/connection.php' ;

$id_ub                  = addslashes( htmlentities( $_POST['id_ub'] ) );
$id_pembeli             = addslashes( htmlentities( $_POST['id_pembeli'] ) );
$id_pengantar           = addslashes( htmlentities( $_POST['id_pengantar'] ) );
$total                  = addslashes( htmlentities( $_POST['total'] ) );
$catatan                = addslashes( htmlentities( $_POST['catatan'] ) );
$status                 = addslashes( htmlentities( $_POST['status'] ) );
$alamat_kirim           = addslashes( htmlentities( $_POST['alamat_kirim'] ) );
$alamat_kirim_latitude  = addslashes( htmlentities( $_POST['alamat_kirim_latitude'] ) );
$alamat_kirim_longitude = addslashes( htmlentities( $_POST['alamat_kirim_longitude'] ) );

$query = "  INSERT INTO transaksi_global SET 
            id_ub                   = '$id_ub',
            id_pembeli              = '$id_pembeli',
            id_pengantar            = '$id_pengantar',
            total                   = '$total',  
            catatan                 = '$catatan',                
            status                  = '$status',               
            alamat_kirim            = '$alamat_kirim',
            alamat_kirim_latitude   = '$alamat_kirim_latitude',
            alamat_kirim_longitude  = '$alamat_kirim_longitude' 
";

$result = mysqli_query( $conn, $query );

$response = array();

if ( $result ) {
    echo json_encode( array( 'message' => 'Sukses menambah data baru.', 'status' => true ) );
} else {
    echo json_encode( array( 'message' => 'Gagal menambah data baru.', 'status' => false ) );
}

?>
