<?php
header( 'Content-Type: application/json' );
require_once '../setting/connection.php';

$query = '  SELECT  t1.*, t2.nama_ub, t2.id_desa, t3.nama_desa, t3.id_daerah, t4.nama_daerah,
                    t5.nama_pengantar, t5.no_hp AS no_hp_pengantar, t5.foto AS foto_pengantar,
                    t6.nama_pembeli, t6.no_hp AS no_hp_pembeli, t6.foto AS foto_pembeli
            FROM transaksi_global t1
                LEFT JOIN ub t2 ON t1.id_ub=t2.id 
                LEFT JOIN desa t3 ON t2.id_desa=t3.id 
                LEFT JOIN daerah t4 ON t3.id_daerah=t4.id
                LEFT JOIN pengantar t5 ON t1.id_pengantar=t5.id
                LEFT JOIN pembeli t6 ON t1.id_pembeli=t6.id
            ';

$result = mysqli_query( $conn, $query ) or die( 'Select Query Failed.' );

$i = 0;

while( $rows = mysqli_fetch_array( $result ) ) {
    $array_data[$i]['id']       = $rows['id'];
    // $array_data[$i]['total']    = $rows['total'];
    $array_data[$i]['catatan']  = $rows['catatan'];
    $array_data[$i]['status']   = $rows['status'];

    $array_data[$i]['ub']['id_ub']      = $rows['id_ub'];
    $array_data[$i]['ub']['nama_ub']    = $rows['nama_ub'];

    $array_data[$i]['desa']['id_desa']      = $rows['id_desa'];
    $array_data[$i]['desa']['nama_desa']    = $rows['nama_desa'];

    $array_data[$i]['daerah']['id_daerah']      = $rows['id_daerah'];
    $array_data[$i]['daerah']['nama_daerah']    = $rows['nama_daerah'];

    $array_data[$i]['pengantar']['id_pengantar']    = $rows['id_pengantar'];
    $array_data[$i]['pengantar']['nama_pengantar']  = $rows['nama_pengantar'];
    $array_data[$i]['pengantar']['no_hp_pengantar'] = $rows['no_hp_pengantar'];
    $array_data[$i]['pengantar']['foto_pengantar']  = $rows['foto_pengantar'];

    $array_data[$i]['pembeli']['id_pembeli']    = $rows['id_pembeli'];
    $array_data[$i]['pembeli']['nama_pembeli']  = $rows['nama_pembeli'];
    $array_data[$i]['pembeli']['no_hp_pembeli'] = $rows['no_hp_pembeli'];
    $array_data[$i]['pembeli']['foto_pembeli']  = $rows['foto_pembeli'];

    $query_produk = "   SELECT t1.id_produk, t2.nama_produk, t1.harga_jual, t1.kuantiti, (t1.harga_jual * t1.kuantiti) AS subtotal, t2.foto
                        FROM transaksi_detail t1
                        LEFT JOIN produk t2 ON t1.id_produk = t2.id
                        WHERE nota='$rows[id]'";
    $result_produk = mysqli_query( $conn, $query_produk ) or die( 'Select Query Produk Failed.' );

    while( $rows_produk = mysqli_fetch_assoc( $result_produk ) ) {
        $array_produk[] = $rows_produk;

        $total = $total + $rows_produk['subtotal'];
    }

    $array_data[$i]['produk']  = $array_produk;

    $array_data[$i]['alamat_kirim']['alamat_kirim']             = $rows['alamat_kirim'];
    $array_data[$i]['alamat_kirim']['alamat_kirim_latitude']    = $rows['alamat_kirim_latitude'];
    $array_data[$i]['alamat_kirim']['alamat_kirim_longitude']   = $rows['alamat_kirim_longitude'];

    $array_data[$i]['total'] = $total;

    $array_data[$i]['created_at'] = $rows['created_at'];
    $array_data[$i]['updated_at'] = $rows['updated_at'];

    $i ++;
}

echo json_encode( $array_data );

?>
