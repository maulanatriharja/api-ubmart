<?php
header( 'Content-Type: application/json' );
require_once '../setting/connection.php';

$query = '  SELECT  t1.*, t1.id_kategori, t2.nama_kategori,
                    t2.id_ub, t3.nama_ub, 
                    t3.id_desa, t4.nama_desa, t4.id_daerah, t5.nama_daerah 
            FROM produk t1
            LEFT JOIN produk_kategori t2 ON t1.id_kategori=t2.id 
            LEFT JOIN ub t3 ON t2.id_ub=t3.id 
            LEFT JOIN desa t4 ON t3.id_desa=t4.id 
            LEFT JOIN daerah t5 ON t4.id_daerah=t5.id';

$result = mysqli_query( $conn, $query ) or die( 'Select Query Failed.' );

$i = 0;

while( $rows = mysqli_fetch_array( $result ) ) {
    $array_data[$i]['id']           = $rows['id'];
    $array_data[$i]['nama_produk']  = $rows['nama_produk'];
    $array_data[$i]['harga_pokok']  = $rows['harga_pokok'];
    $array_data[$i]['harga_jual']   = $rows['harga_jual'];
    $array_data[$i]['keterangan']   = $rows['keterangan'];
    $array_data[$i]['foto']         = $rows['foto'];

    $array_data[$i]['kategori']['id_kategori']      = $rows['id_kategori'];
    $array_data[$i]['kategori']['nama_kategori']    = $rows['nama_kategori'];

    $array_data[$i]['ub']['id_ub']      = $rows['id_ub'];
    $array_data[$i]['ub']['nama_ub']    = $rows['nama_ub'];

    $array_data[$i]['desa']['id_desa']      = $rows['id_desa'];
    $array_data[$i]['desa']['nama_desa']    = $rows['nama_desa'];

    $array_data[$i]['daerah']['id_daerah']      = $rows['id_daerah'];
    $array_data[$i]['daerah']['nama_daerah']    = $rows['nama_daerah'];

    $array_data[$i]['created_at'] = $rows['created_at'];
    $array_data[$i]['updated_at'] = $rows['updated_at'];

    $i ++;
}

echo json_encode( $array_data );

?>
