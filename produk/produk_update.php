<?php
header( 'Content-Type: application/json' );
require_once '../setting/connection.php';

$id             = addslashes( htmlentities( $_POST['id'] ) );
$id_kategori    = addslashes( htmlentities( $_POST['id_kategori'] ) );
$nama_produk    = addslashes( htmlentities( $_POST['nama_produk'] ) );
$harga_pokok    = addslashes( htmlentities( $_POST['harga_pokok'] ) );
$harga_jual     = addslashes( htmlentities( $_POST['harga_jual'] ) );
$keterangan     = addslashes( htmlentities( $_POST['keterangan'] ) );
$foto           = addslashes( htmlentities( $_POST['foto'] ) );

$query = "  UPDATE produk SET 
            id_kategori = '$id_kategori',
            nama_produk = '$nama_produk',
            harga_pokok = '$harga_pokok',
            harga_jual  = '$harga_jual',
            keterangan  = '$keterangan',
            foto        = '$foto'	 	
            WHERE id    = '$id'";

if ( mysqli_query( $conn, $query ) or die( 'Update Query Failed' ) ) {
    echo json_encode( array( 'message' => 'Sukses mengubah data baru.', 'status' => true ) );
} else {
    echo json_encode( array( 'message' => 'Gagal mengubah data baru.', 'status' => false ) );
}

?>
