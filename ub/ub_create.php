<?php
header( 'Content-Type: application/json' );
require_once '../setting/connection.php' ;

$id_desa = addslashes( htmlentities( $_POST['id_desa'] ) );
$nama_ub = addslashes( htmlentities( $_POST['nama_ub'] ) );
$foto = addslashes( htmlentities( $_POST['foto'] ) );

$query = "INSERT INTO ub SET id_desa='$id_desa', nama_ub='$nama_ub', foto='$foto'";

$result = mysqli_query( $conn, $query );

$response = array();

if ( $result ) {
    echo json_encode( array( 'message' => 'Sukses menambah data baru.', 'status' => true ) );
} else {
    echo json_encode( array( 'message' => 'Gagal menambah data baru.', 'status' => false ) );
}

?>
