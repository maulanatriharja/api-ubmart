<?php
header( 'Content-Type: application/json' );
require_once '../setting/connection.php';

$query = '  SELECT t1.*, t2.nama_desa, t2.id_daerah, t3.nama_daerah FROM ub t1 
            LEFT JOIN desa t2 ON t1.id_desa=t2.id 
            LEFT JOIN daerah t3 ON t2.id_daerah=t3.id';

$result = mysqli_query( $conn, $query ) or die( 'Select Query Failed.' );

$i = 0;

while( $rows = mysqli_fetch_array( $result ) ) {
    $array_data[$i]['id'] = $rows['id'];
    $array_data[$i]['nama_ub'] = $rows['nama_ub'];

    $array_data[$i]['desa']['id_desa'] = $rows['id_desa'];
    $array_data[$i]['desa']['nama_desa'] = $rows['nama_desa'];

    $array_data[$i]['daerah']['id_daerah'] = $rows['id_daerah'];
    $array_data[$i]['daerah']['nama_daerah'] = $rows['nama_daerah'];

    $array_data[$i]['created_at'] = $rows['created_at'];
    $array_data[$i]['updated_at'] = $rows['updated_at'];

    $i ++;
}

echo json_encode( $array_data );

?>
